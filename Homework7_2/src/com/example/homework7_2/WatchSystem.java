package com.example.homework7_2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import android.util.Log;
import android.widget.Toast;

public class WatchSystem extends BroadcastReceiver {
	int mCount = 0;
	static final int G_NOTIFY_NUM = 1; // 식별ID
	NotificationManager m_NotiManager;

	@Override
	public void onReceive(Context context, Intent intent) {
		String act = intent.getAction();
		mCount++;
		m_NotiManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		if (act.equals(Intent.ACTION_BATTERY_CHANGED)) {
			Toast.makeText(context, "Battery is changed", Toast.LENGTH_SHORT)
					.show();
			Notification noti = new Notification(R.drawable.ic_launcher,
					"Battery is changed", System.currentTimeMillis());
			noti.flags |= Notification.FLAG_AUTO_CANCEL;
			noti.when = System.currentTimeMillis();
			noti.icon = R.drawable.ic_launcher;
			
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent content = PendingIntent.getActivity(context, 0,
					intent, 0);
			noti.setLatestEventInfo(context, "*Battery*", "Battery is changed",
					content);
			m_NotiManager.notify(WatchSystem.G_NOTIFY_NUM, noti);

		} else if (act.equals(Intent.ACTION_BATTERY_LOW)) {
			Toast.makeText(context, "Battery is low", Toast.LENGTH_SHORT)
					.show();
			Notification noti = new Notification(R.drawable.ic_launcher,
					"Battery is low", System.currentTimeMillis());
			noti.defaults |= (Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE); // 진동사용
			noti.flags |= Notification.FLAG_AUTO_CANCEL;
			noti.when = System.currentTimeMillis();
			noti.icon = R.drawable.ic_launcher;
			Intent intent1 = new Intent(context, Main.class);
			intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent1);
			PendingIntent content = PendingIntent.getActivity(context, 0,
					intent1, 0);
			noti.setLatestEventInfo(context, "*Battery*", "Battery is low",
					content);
			m_NotiManager.notify(WatchSystem.G_NOTIFY_NUM, noti);
			
		} else if (act.equals(Intent.ACTION_BATTERY_OKAY)) {
			Toast.makeText(context, "Battery is okay", Toast.LENGTH_SHORT)
					.show();
			Notification noti = new Notification(R.drawable.ic_launcher,
					"Battery is okay", System.currentTimeMillis());
			noti.flags |= Notification.FLAG_AUTO_CANCEL;
			noti.when = System.currentTimeMillis();
			noti.icon = R.drawable.ic_launcher;
			
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent content = PendingIntent.getActivity(context, 0,
					intent, 0);
			noti.setLatestEventInfo(context, "*Battery*", "Battery is okay",
					content);
			m_NotiManager.notify(WatchSystem.G_NOTIFY_NUM, noti);
			
		} else if (act.equals(Intent.ACTION_POWER_CONNECTED)) {
			Toast.makeText(context, "Power is connected", Toast.LENGTH_SHORT)
					.show();
			Notification noti = new Notification(R.drawable.ic_launcher,
					"Power is connected", System.currentTimeMillis());
			noti.flags |= Notification.FLAG_AUTO_CANCEL;
			noti.when = System.currentTimeMillis();
			noti.icon = R.drawable.ic_launcher;
			
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent content = PendingIntent.getActivity(context, 0,
					intent, 0);
			noti.setLatestEventInfo(context, "*Power*", "Power is connected",
					content);
			m_NotiManager.notify(WatchSystem.G_NOTIFY_NUM, noti);
			
		} else if (act.equals(Intent.ACTION_POWER_DISCONNECTED)) {
			Toast.makeText(context, "Power is disconnected", Toast.LENGTH_SHORT)
					.show();
			Notification noti = new Notification(R.drawable.ic_launcher,
					"Power is disconnected", System.currentTimeMillis());
			noti.flags |= Notification.FLAG_AUTO_CANCEL;
			noti.when = System.currentTimeMillis();
			noti.icon = R.drawable.ic_launcher;
			
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent content = PendingIntent.getActivity(context, 0,
					intent, 0);
			noti.setLatestEventInfo(context, "*Power*", "Power is disconnected",
					content);
			m_NotiManager.notify(WatchSystem.G_NOTIFY_NUM, noti);
		} else if (act.equals(Intent.ACTION_MEDIA_MOUNTED)) {
			Toast.makeText(context, "Media is mounted", Toast.LENGTH_SHORT)
					.show();
			Notification noti = new Notification(R.drawable.ic_launcher,
					"Media is mounted", System.currentTimeMillis());
			noti.flags |= Notification.FLAG_AUTO_CANCEL;
			noti.when = System.currentTimeMillis();
			noti.icon = R.drawable.ic_launcher;
			
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent content = PendingIntent.getActivity(context, 0,
					intent, 0);
			noti.setLatestEventInfo(context, "*Media*", "Media is mounted",
					content);
			m_NotiManager.notify(WatchSystem.G_NOTIFY_NUM, noti);
		} else if (act.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
			Toast.makeText(context, "Media is unmounted", Toast.LENGTH_SHORT)
					.show();
			Notification noti = new Notification(R.drawable.ic_launcher,
					"Media is unmounted", System.currentTimeMillis());
			noti.flags |= Notification.FLAG_AUTO_CANCEL;
			noti.when = System.currentTimeMillis();
			noti.icon = R.drawable.ic_launcher;
			
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent content = PendingIntent.getActivity(context, 0,
					intent, 0);
			noti.setLatestEventInfo(context, "*Media*", "Media is unmounted",
					content);
			m_NotiManager.notify(WatchSystem.G_NOTIFY_NUM, noti);
		}
	}

}
