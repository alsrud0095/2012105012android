package vr.practice7_4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Main extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}
	
	public void onClickSend1(View v) {
		Intent i = new Intent();
		i.setAction("com.broadcast.ANDROID1");
		sendBroadcast(i);
	}
	
	public void onClickSend2(View v) {
		Intent i = new Intent();
		i.setAction("com.broadcast.ANDROID2");
		sendBroadcast(i);
	}
	
	public void onClickSend3(View v) {
		Intent i = new Intent();
		i.setAction("com.broadcast.ANDROID3");
		sendBroadcast(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
