package com.example.homework;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Act2 extends Activity {

	/** Called when the activity is first created. */
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.act2);
	    
	    WebView webview = (WebView)findViewById(R.id.webView1);
	    webview.getSettings().setJavaScriptEnabled(true);
	    webview.loadUrl("http://blog.naver.com/soon24525");
	    webview.setWebViewClient(new NewWebViewClient());
	}

}

class NewWebViewClient extends WebViewClient {
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		view.loadUrl(url);
		
		return true;
	}
}