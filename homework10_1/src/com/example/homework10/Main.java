package com.example.homework10;

import com.example.homework10_1.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main extends Activity {
	static {
		System.loadLibrary("Calculate");
	}

	public native double add(double x, double y);

	public native double mul(double x, double y);

	public native double div(double x, double y);
	
	public native double sub(double x, double y);

	TextView display;
	String operator = "";
	String txt ="";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		display = (TextView) findViewById(R.id.TextView1);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			txt += "7";
			break;
		case R.id.button2:
			txt += "8";
			break;
		case R.id.button3:
			txt += "9";
			break;
		case R.id.button4:
			operator = "*";
			txt += " * ";
			break;
		case R.id.button5:
			txt += "4";
			break;
		case R.id.button6:
			txt += "5";
			break;
		case R.id.button7:
			txt += "6";
			break;
		case R.id.button8:
			operator = "/";
			txt += " / ";
			break;
		case R.id.button9:
			txt += "1";
			break;
		case R.id.button10:
			txt += "2";
			break;
		case R.id.button11:
			txt += "3";
			break;
		case R.id.button12:
			operator = "+";
			txt += " + ";
			break;
		case R.id.button13:
			txt += " . ";
			break;
		case R.id.button14:
			txt += "0";
			break;
		case R.id.button16:
			operator = "-";
			txt += " - ";
			break;
		case R.id.button15:
			txt += " = ";
			String input = display.getText().toString();
			String v1 = input.substring(0, input.indexOf(operator));
			String v2 = input.substring(input.indexOf(operator)+1, input.length());
			double res=0.0;

			if(operator.equals("+")) {
				res = add(Double.parseDouble(v1), Double.parseDouble(v2));
			}
			else if(operator.equals("-")) {
				res = sub(Double.parseDouble(v1), Double.parseDouble(v2));
			}
			else if(operator.equals("/")) {
				res = div(Double.parseDouble(v1), Double.parseDouble(v2));
			}
			else {
				res = mul(Double.parseDouble(v1), Double.parseDouble(v2));
			}
			
			if(res > (int)res) {
				txt += res; 
			}
			else {
				txt += (int)res;
			}
		}
		display.setText(txt);
	}
}