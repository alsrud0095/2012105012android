#include <jni.h>
JNIEXPORT jdouble JNICALL Java_com_example_homework10_Main_add(JNIEnv *env,
		jobject obj, jdouble value1, jdouble value2) {
	return (value1 + value2);
}
JNIEXPORT jdouble JNICALL Java_com_example_homework10_Main_mul(JNIEnv *env,
		jobject obj, jdouble value1, jdouble value2) {
	return (value1 * value2);
}
JNIEXPORT jdouble JNICALL Java_com_example_homework10_Main_div(JNIEnv *env,
		jobject obj, jdouble value1, jdouble value2) {
	return (value1 / value2);
}
JNIEXPORT jdouble JNICALL Java_com_example_homework10_Main_sub(JNIEnv *env,
		jobject obj, jdouble value1, jdouble value2) {
	return (value1 - value2);
}
