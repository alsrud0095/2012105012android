package vr.ex6_callcp;

import android.app.Activity;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class UpAct extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
		setContentView(R.layout.update);
	}

	public void onClickButton3(View v) {
		ContentValues values = new ContentValues();

		TextView txt = (TextView) findViewById(R.id.editText1);
		String name = txt.getText().toString();
		txt = (TextView) findViewById(R.id.editText2);
		String age = txt.getText().toString();

		values.put("age", age);
		getContentResolver().update(
				Uri.parse("content://ex5_databasesave.test/people/" + name),
				values, null, null);
		Toast.makeText(UpAct.this, "���� ����!", Toast.LENGTH_SHORT).show();
		finish();
	}
}