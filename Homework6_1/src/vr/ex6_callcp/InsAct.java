package vr.ex6_callcp;

import android.app.Activity;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class InsAct extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
		setContentView(R.layout.insert);
	}

	public void onClickButton1(View v) {
		ContentValues values = new ContentValues();

		TextView txt = (TextView) findViewById(R.id.editText1);
		String name = txt.getText().toString();
		txt = (TextView) findViewById(R.id.editText2);
		String age = txt.getText().toString();

		values.put("name", name);
		values.put("age", age);

		getContentResolver().insert(
				Uri.parse("content://ex5_databasesave.test/people"), values);
		
		Toast.makeText(InsAct.this, "�߰� ����!", Toast.LENGTH_SHORT).show();
		finish();
	}
}