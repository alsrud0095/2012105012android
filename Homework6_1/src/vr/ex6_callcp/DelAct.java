package vr.ex6_callcp;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DelAct extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
		setContentView(R.layout.delete);
	}

	public void onClickButton2(View v) {

		TextView txt = (TextView)findViewById(R.id.editText3);
		String name = txt.getText().toString();

		getContentResolver().delete(Uri.parse("content://ex5_databasesave.test/people/"+name),null , null);
		
		Toast.makeText(DelAct.this, "���� ����!", Toast.LENGTH_SHORT).show();
		finish();
	}
}