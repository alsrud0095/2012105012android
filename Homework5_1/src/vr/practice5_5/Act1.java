package vr.practice5_5;

import vr.practice5_5.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Act1 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act1);
	}

	public void showListAct(View v) {
		Intent i = new Intent(this, ListAct.class);
		startActivity(i);
	}

	public void showInsAct(View v) {
		Intent i = new Intent(this, InsAct.class);
		startActivity(i);
	}

	public void showDelAct(View v) {
		Intent i = new Intent(this, DelAct.class);
		startActivity(i);
	}

	public void showSqlAct(View v) {
		Intent i = new Intent(this, SqlAct.class);
		startActivity(i);
	}
	
	public void showUpAct(View v) {
		Intent i = new Intent(this, UpAct.class);
		startActivity(i);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.act1, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
