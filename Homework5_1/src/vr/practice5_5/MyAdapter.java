package vr.practice5_5;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class MyAdapter extends CursorAdapter {

	public MyAdapter(Context context, Cursor c) {
		super(context, c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View newView(Context context, Cursor c, ViewGroup parent) {
		// TODO Auto-generated method stub
		//LayoutInflater inflater = LayoutInflater.from(context);
		return View.inflate(context, android.R.layout.simple_list_item_2, null);
	}

	@Override
	public void bindView(View v, Context context, Cursor c) {
		// TODO Auto-generated method stub
		TextView info1 = (TextView)v.findViewById(android.R.id.text1);
		TextView info2 = (TextView)v.findViewById(android.R.id.text2);
		info1.setText(c.getString(1));
		info2.setText(c.getString(2)+" "+c.getString(3));
	}

}
