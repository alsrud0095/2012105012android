package vr.practice5_5;

import vr.practice5_5.R;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class UpAct extends ListActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upact);
		loadDB();
	}

	public void onResume() {
		super.onResume();
		loadDB();
	}
	
	public void loadDB() {
		ListView list = (ListView)findViewById(android.R.id.list);
		SQLiteDatabase db = openOrCreateDatabase(
				 "test.db",
		 SQLiteDatabase.CREATE_IF_NECESSARY,
		 null);
		db.execSQL("CREATE TABLE IF NOT EXISTS people " +
				 "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				 ""+ "stn TEXT UNIQUE on conflict replace," + "name TEXT," + "age INTEGER);");
		 
		 Cursor c = db.rawQuery("SELECT * FROM people;", null);
		 startManagingCursor(c);	 
		 
		 MyAdapter adapt = new MyAdapter(UpAct.this, c);
				 
		 list.setAdapter(adapt);
		 
		 if (db != null) {
			 db.close();
		 }
	}
	
	
	public void onClickButton(View v) {
		EditText txt = null;
		txt = (EditText) findViewById(R.id.editText0);
		String stn = txt.getText().toString();		
		txt = (EditText) findViewById(R.id.editText1);
		String name = txt.getText().toString();
		txt = (EditText) findViewById(R.id.editText2);
		String age = txt.getText().toString();
		

		if(name.isEmpty())
		{
			String sql = "UPDATE people SET age="+ age +" WHERE stn='"+ stn +"'";
			SQLiteDatabase db = openOrCreateDatabase("test.db",
					SQLiteDatabase.CREATE_IF_NECESSARY, null);
			db.execSQL(sql);
		}
		else if(age.isEmpty())
		{
			String sql = "UPDATE people SET name='"+ name +"' WHERE stn='"+ stn +"'";
			SQLiteDatabase db = openOrCreateDatabase("test.db",
					SQLiteDatabase.CREATE_IF_NECESSARY, null);
			db.execSQL(sql);
		}		
		else if(name.toString().length()>20)
		{
			Toast msg =	Toast.makeText(UpAct.this, 
					"이름은 20자 이내로 입력하세요!", Toast.LENGTH_SHORT);
			msg.show();
		}
		else if(Integer.parseInt(age)>200)
		{
			Toast msg =	Toast.makeText(UpAct.this, 
					"나이는 200 이하로 입력하세요!", Toast.LENGTH_SHORT);		
			msg.show();
		}				
		else			
		{	
			String sql = "UPDATE people SET name='" + name + "', age="+ age +" WHERE stn='"+ stn +"'";
			SQLiteDatabase db = openOrCreateDatabase("test.db",
					SQLiteDatabase.CREATE_IF_NECESSARY, null);
			db.execSQL(sql);
		}
		Toast.makeText(UpAct.this,"수정 성공!", Toast.LENGTH_SHORT).show();
		onResume();
	}
}
