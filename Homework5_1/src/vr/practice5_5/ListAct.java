package vr.practice5_5;

import vr.practice5_5.R;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class ListAct extends ListActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list);
		loadDB();
	}

	@Override
	public void onResume() {
		super.onResume();
		loadDB();
	}
	
	public void loadDB() {
		ListView list = (ListView)findViewById(android.R.id.list);
		SQLiteDatabase db = openOrCreateDatabase(
				 "test.db",
		 SQLiteDatabase.CREATE_IF_NECESSARY,
		 null);
		
		db.execSQL("CREATE TABLE IF NOT EXISTS people " +
				 "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				 ""+ "stn TEXT not null UNIQUE," + "name TEXT," + "age INTEGER);");
		 
		 Cursor c = db.rawQuery("SELECT * FROM people;", null);
		 startManagingCursor(c);	 
		 
		 MyAdapter adapt = new MyAdapter(ListAct.this, c);
				 
		 list.setAdapter(adapt);
		 
		 if (db != null) {
			 db.close();
		 }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
