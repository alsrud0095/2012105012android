package vr.practice5_5;

import vr.practice5_5.R;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

public class SqlAct extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sqlact);
		loadDB();
	}

	public void onResume() {
		super.onResume();
		loadDB();
	}
	
	public void loadDB() {
		ListView list = (ListView)findViewById(android.R.id.list);
		SQLiteDatabase db = openOrCreateDatabase(
				 "test.db",
		 SQLiteDatabase.CREATE_IF_NECESSARY,
		 null);
		db.execSQL("CREATE TABLE IF NOT EXISTS people " +
				 "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				 ""+ "stn TEXT UNIQUE on conflict replace," + "name TEXT," + "age INTEGER);");
		 
		 Cursor c = db.rawQuery("SELECT * FROM people;", null);
		 startManagingCursor(c);	 
		 
		 MyAdapter adapt = new MyAdapter(SqlAct.this, c);
				 
		 list.setAdapter(adapt);
		 
		 if (db != null) {
			 db.close();
		 }
	}
	
	public void onClickButton(View v) {
		EditText txt = null;
		txt = (EditText) findViewById(R.id.editText4);
		String c_query = txt.getText().toString();
		String sql = c_query;
		SQLiteDatabase db = openOrCreateDatabase("test.db",
				SQLiteDatabase.CREATE_IF_NECESSARY, null);
		db.execSQL(sql);
		onResume();
	}
}