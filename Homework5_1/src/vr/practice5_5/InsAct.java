package vr.practice5_5;

import java.util.Calendar;

import vr.practice5_5.R;
import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class InsAct extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.insact);
	}

	public void onClickButton(View v) {
		EditText txt = null;
		Calendar cal = Calendar.getInstance();
		txt = (EditText) findViewById(R.id.editText0);
		String stn = txt.getText().toString();
		txt = (EditText) findViewById(R.id.editText1);
		String name = txt.getText().toString();	
		txt = (EditText) findViewById(R.id.editText2);
		String age = txt.getText().toString();
		
		int year = cal.get(Calendar.YEAR);
		int cnt1 = stn.toString().length();
		int cnt2 = name.toString().length();
		int val_age = Integer.parseInt(age);		
		
		if(cnt1!=10)
		{
			Toast msg =	Toast.makeText(InsAct.this, 
					"학번은 10자리 입니다.!", Toast.LENGTH_SHORT);
			msg.show();
		}
		else if(Integer.parseInt(stn.substring(0,4))>year)
		{
			Toast msg =	Toast.makeText(InsAct.this, 
					"학번 앞 4자리는 실제년도 입니다.!", Toast.LENGTH_SHORT);
			msg.show();
		}
		else if(cnt2>20)
		{
			Toast msg =	Toast.makeText(InsAct.this, 
					"이름은 20자 이내로 입력하세요!", Toast.LENGTH_SHORT);
			msg.show();
		}
		else if(val_age>200)
		{
			Toast msg =	Toast.makeText(InsAct.this, 
					"나이는 200 이하로 입력하세요!", Toast.LENGTH_SHORT);		
			msg.show();
		}				
		else
		{
			try
			{
			String sql = "INSERT INTO people (stn,name,age) VALUES ('" + stn + "','" + name + "',"+ age + ");";
			SQLiteDatabase db = openOrCreateDatabase("test.db",
					SQLiteDatabase.CREATE_IF_NECESSARY, null);
			db.execSQL(sql);
			Toast.makeText(InsAct.this, "입력 성공!", Toast.LENGTH_SHORT).show();
			finish();
			} catch(Exception e) {Toast.makeText(InsAct.this,
					"동일한 학번이 있습니다!", Toast.LENGTH_SHORT).show();}
		}	
	}
}
