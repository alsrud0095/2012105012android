package vr.homework4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class Act1 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act1);
    }

    public void onButtonClick(View v) {
    	EditText text1 = (EditText)findViewById(R.id.EditText1);
    	EditText text2 = (EditText)findViewById(R.id.EditText2);
    	Intent intent1 = new Intent(Act1.this, Act2.class);
    	intent1.putExtra("name", text1.getText().toString());
    	intent1.putExtra("number", text2.getText().toString());
    	startActivity(intent1);
    	
    	
    
    
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.act1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
