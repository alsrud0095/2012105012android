package vr.homework4;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class Act2 extends Activity {
	String input1;
	String input2;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.act2);
	    // TODO Auto-generated method stub
	    input1 = getIntent().getStringExtra("name");
	    input2 = getIntent().getStringExtra("number");
	    if(input1!=null) {
	    	TextView txt1 = (TextView)findViewById(R.id.textView2);
	    	txt1.setText(":" + input1);
	    }
	    if(input2!=null) {
	    	TextView txt2 = (TextView)findViewById(R.id.textView4);
	    	txt2.setText(":" + input2);
	    }
	  
}
	
	public void onButtonClick(View v) {
		Intent intent1;
		intent1 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + input2));
		startActivity(intent1);
			
	}

}